# Testing the pipeline bot

The pipeline bot has integration tests that must be run before merging any changes.

To run them, you need to have access to a https://gitlab.com account that is not member of the CKI team.
Then, define the following environment variables:

- `IT_GITLAB_MR_URL`: https://gitlab.com
- `IT_GITLAB_MR_MEMBER_TOKEN`: your personal access token at https://gitlab.com
- `IT_GITLAB_MR_CONTRIBUTOR_TOKEN`: a personal access token for the non-member
- `IT_GITLAB_MR_PROJECT_NAME`: cki-project/pipeline-definition
- `IT_GITLAB_MR_PROJECT_URL`: https://gitlab.com/cki-project/pipeline-definition
- `IT_GITLAB_PIPELINES_URL`: `xci32` URL
- `IT_GITLAB_PIPELINES_TOKEN`: your personal access token at `xci32`
- `IT_GITLAB_PIPELINES_PROJECT_CKI`: `cki-project/cki-pipeline`
- `IT_GITLAB_PIPELINES_TRIGGER_CKI`: trigger token for the cki-pipeline project
- `IT_GITLAB_PIPELINES_PROJECT_BREW`: `cki-project/brew-pipeline`
- `IT_GITLAB_PIPELINES_TRIGGER_BREW`: trigger token for the brew-pipeline project

You can run the integration tests with

```shell
# run all tests
python3 -m unittest inttests.test_pipeline_bot -v
# run an individual test
python3 -m unittest inttests.test_pipeline_bot.TestPipelineBot.test_member -v
```
