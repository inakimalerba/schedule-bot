"""Delete PODs orphaned by gitlab-runner."""
import datetime
import json
import os
import pathlib
import traceback

import dateutil
import gitlab
import kubernetes
import requests

from schedule_bot import utils


class KubernetesHelper:
    """Provide some Kubernetes helper functions."""

    secrets_path = '/var/run/secrets/kubernetes.io/serviceaccount'

    def __init__(self):
        """Create a new Kubernetes helper."""
        self.config = kubernetes.client.Configuration()
        self.config_type = None
        self.namespace = None

    def setup(self):
        """Set up the configuration to allow API calls."""
        if 'OPENSHIFT_KEY' in os.environ:
            self._setup_environment()
        elif os.path.isdir(self.secrets_path):
            self._setup_cluster()
        else:
            self._setup_kube_config()

    def api(self):
        """Return a Kubernetes API client."""
        client = kubernetes.client.ApiClient(configuration=self.config)
        return kubernetes.client.CoreV1Api(client)

    def _setup_environment(self):
        self.config_type = 'OPENSHIFT_* environment variables'
        self._update_config(os.environ['OPENSHIFT_SERVER'],
                            os.environ['REQUESTS_CA_BUNDLE'],
                            os.environ['OPENSHIFT_KEY'])
        self.namespace = os.environ['OPENSHIFT_PROJECT']

    def _setup_cluster(self):
        self.config_type = 'cluster configuration'
        self._update_config((f'https://{os.environ["KUBERNETES_SERVICE_HOST"]}'
                             f':{os.environ["KUBERNETES_SERVICE_PORT"]}'),
                            os.path.join(self.secrets_path, 'ca.crt'),
                            self._get_secret('token'))
        self.namespace = self._get_secret('namespace')

    def _setup_kube_config(self):
        self.config_type = '~/.kube/config'
        kubernetes.config.load_kube_config(client_configuration=self.config)
        config_contexts = kubernetes.config.list_kube_config_contexts()
        self.config.ssl_ca_cert = os.environ['REQUESTS_CA_BUNDLE']
        self.namespace = config_contexts[1]['context']['namespace']

    def _update_config(self, server, ca_crt, key):
        self.config.api_key_prefix['authorization'] = 'Bearer'
        self.config.api_key['authorization'] = key
        self.config.host = server
        self.config.ssl_ca_cert = ca_crt

    def _get_secret(self, name):
        return pathlib.Path(self.secrets_path, name).read_text('utf-8')


class GitLabRunnerPod:
    """Manage a pod spawned by gitlab-runner."""

    def __init__(self, api, pod, gitlab_tokens):
        """Initialize the pod and parse environment variables."""
        self._api = api
        self._pod = pod
        self.name = self._pod.metadata.name

        env = {x.name: x.value
               for x in self._pod.spec.containers[0].env}
        token = os.environ[gitlab_tokens[env['CI_SERVER_HOST']]]
        self._gitlab = gitlab.Gitlab(f'https://{env["CI_SERVER_HOST"]}', token)
        self.project_path = env['CI_PROJECT_PATH']
        self._job_id = env['CI_JOB_ID']
        self.job_url = env['CI_JOB_URL']

    def job_status(self):
        """Check whether the GitLab job is still running."""
        job = self._get_job()
        if not job:
            # project exists, but job not? try again just to be sure
            job = self._get_job()
        if not job:
            return 'non-existing'
        if not job.finished_at:
            return 'not finished'
        now = datetime.datetime.now(datetime.timezone.utc)
        finished = dateutil.parser.parse(job.finished_at)
        # wait 5 minutes for the runner to clean up after itself
        if now - finished < datetime.timedelta(minutes=5):
            return 'not finished'
        return 'finished'

    def delete(self):
        """Delete the pod."""
        self._api.delete_namespaced_pod(name=self.name,
                                        namespace=self._pod.metadata.namespace,
                                        grace_period_seconds=0)

    def _get_job(self):
        """If the project exists, return the job or None; otherwise, throw."""
        project = self._gitlab.projects.get(self.project_path)
        try:
            return project.jobs.get(self._job_id)
        except gitlab.exceptions.GitlabGetError:
            return None


def shorten_url(url):
    """Attempt to shorten a URL."""
    try:
        if 'SHORTENER_URL' in os.environ:
            response = requests.post(
                os.environ['SHORTENER_URL'], data={'url': url})
            response.raise_for_status()
            url = response.text.strip()
    except Exception:  # pylint: disable=broad-except
        traceback.print_exc()
    return url


def notify_pod(pod, status, delete):
    """Send a message to the IRC bot."""
    if 'IRCBOT_URL' not in os.environ:
        return
    action = 'Deleting' if delete else 'Detected'
    url = shorten_url(pod.job_url)
    message = (f'👻 {action} abandoned pod {pod.name} from {status} job '
               f'of {pod.project_path} {url}')
    requests.post(os.environ['IRCBOT_URL'], json={'message': message})


def main(config):
    """Delete PODs orphaned by gitlab-runner."""
    utils.cprint(utils.Colors.YELLOW, 'Loading configuration...')
    k8s_helper = KubernetesHelper()
    k8s_helper.setup()
    api = k8s_helper.api()
    print(f'  using {k8s_helper.config_type}')

    utils.cprint(utils.Colors.YELLOW, 'Getting pods...')
    all_pods = api.list_namespaced_pod(k8s_helper.namespace).items
    runner_pods = [p for p in all_pods
                   if p.metadata.name.startswith('runner-')]
    print(f'  Found {len(runner_pods)} gitlab-runner pods ' +
          f'out of {len(all_pods)} pods in total')

    for runner_pod in runner_pods:
        try:
            pod = GitLabRunnerPod(api, runner_pod, config['gitlab_tokens'])
            utils.cprint(utils.Colors.YELLOW, f'Checking {pod.name}...')
            print(f'  {pod.job_url} ', end='')
            status = pod.job_status()
            if status == 'not finished':
                utils.cprint(utils.Colors.GREEN, status)
            else:
                utils.cprint(utils.Colors.RED, status)
                delete = config.get('action', 'report') == 'delete'
                notify_pod(pod, status, delete)
                if delete:
                    print('  Deleting pod... ')
                    pod.delete()
        except Exception:  # pylint: disable=broad-except
            traceback.print_exc()


main(json.loads(os.environ['SCHEDULE_BOT_CONFIG']))
