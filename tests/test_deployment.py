"""Deployment unit tests."""
import os
import unittest
from unittest import mock

from schedule_bot import utils


class TestDeployment(unittest.TestCase):
    @mock.patch.dict(os.environ, {'key1': 'value1', 'key2': 'value2'})
    def test_resolve_variables(self):
        """Check nested config variable replacements."""
        data = {
            'key': 'value',
            'key1': '$key1',
            'key3': {'key4': {'key2': '$key2', 'key5': 'value5'}},
            'key6': ['$key2']
        }
        utils.Deployment({}, {}).resolve_variables(data)
        self.assertEqual(data, {
            'key': 'value',
            'key1': 'value1',
            'key3': {'key4': {'key2': 'value2', 'key5': 'value5'}},
            'key6': ['value2']
        })
