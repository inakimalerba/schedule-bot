"""Deployment of a schedule to OpenShift."""
import json
import subprocess

from . import utils


class OpenShiftDeployment(utils.Deployment):
    """Control the deployment of a schedule to OpenShift."""

    def __init__(self, schedule_config, extra_env_variables):
        """Prepare the deployment of a schedule to OpenShift."""
        super().__init__(schedule_config, extra_env_variables)

        self._schedule_app = f'schedule-bot-{self._schedule_config["name"]}'
        self._schedule_labels = {'app': self._schedule_app}

    def deploy(self):
        """Update the schedule on OpenShift."""
        secrets = self.schedule_secrets()
        secret_refs = [{
            'name': k,
            'valueFrom': {
                'secretKeyRef': {
                    'name': self._schedule_app,
                    'key': k,
                },
            },
        } for k in secrets]

        secrets = [{
            'apiVersion': 'v1',
            'kind': 'Secret',
            'metadata': {
                'name': self._schedule_app,
                'labels': self._schedule_labels,
            },
            'stringData': secrets,
        }]

        cron_jobs = [self._cron_job(job_name, secret_refs)
                     for job_name in self._schedule_config.get('jobs', {})]

        item_list = json.dumps({
            'apiVersion': 'v1',
            'kind': 'List',
            'items': secrets + cron_jobs,
        })

        subprocess.run(['oc', 'apply', '-f', '-'], check=True,
                       input=item_list, encoding='utf-8')

    def _cron_job(self, job_name, secret_refs):
        job_config = self._schedule_config['jobs'][job_name]
        schedule_job = f'{self._schedule_app}-{job_name}'
        job_labels = dict(self._schedule_labels)
        job_labels.update({'schedule_job': schedule_job})

        variables = self.schedule_variables()
        variables.update(self.job_variables(job_name))

        variables = [{'name': k, 'value': v} for k, v in
                     variables.items()]

        # at least 1 CPU if not specified
        kubernetes_config = job_config.get('kubernetes', {})
        resources = {'cpu': int(kubernetes_config.get('cpu', '1'))}
        if 'memory' in kubernetes_config:
            resources['memory'] = kubernetes_config['memory']

        job_labels.update(kubernetes_config.get('labels', {}))

        data_volume = kubernetes_config.get('dataVolume', 'emptyDir')
        if data_volume == 'emptyDir':
            volume = {'emptyDir': {}}
        elif data_volume == 'memory':
            volume = {'emptyDir': {'medium': 'Memory'}}
        else:
            volume = {'persistentVolumeClaim': {'claimName': data_volume}}
        volume['name'] = schedule_job

        pod_spec = {
            'restartPolicy': 'Never',
            'containers': [{
                'name': job_name,
                'image': self.image(job_name),
                'command': self.command_line(job_name),
                'env': secret_refs + variables,
                'volumeMounts': [{
                    'mountPath': '/data',
                    'name': schedule_job,
                }],
                'resources': {
                    'limits': resources,
                    'requests': resources,
                },
            }],
            'volumes': [volume],
        }

        if 'activeDeadlineSeconds' in kubernetes_config:
            pod_spec['activeDeadlineSeconds'] = \
                kubernetes_config['activeDeadlineSeconds']

        return {
            'apiVersion': 'batch/v1beta1',
            'kind': 'CronJob',
            'metadata': {
                'name': schedule_job,
                'labels': job_labels,
            },
            'spec': {
                'schedule': job_config['schedule'],
                'concurrencyPolicy': kubernetes_config.get(
                    'concurrencyPolicy', 'Replace'),
                'suspend': job_config.get('disabled'),
                'jobTemplate': {'spec': {'template': {
                    'metadata': {
                        'name': schedule_job,
                        'labels': job_labels,
                    },
                    'spec': pod_spec,
                }}},
            },
        }

    def cleanup(self):
        """Remove old jobs that are not in the schedule anymore."""
        args = ['oc', 'get', 'all', '-l', f'app={self._schedule_app}', '-o',
                'custom-columns=LABEL:.metadata.labels.schedule_job']
        output = subprocess.run(args, check=True, capture_output=True,
                                encoding='utf-8').stdout

        deployed_jobs = set(f'{self._schedule_app}-{j}' for j in
                            self._schedule_config.get('jobs', {}).keys())
        existing_jobs = set(output.split('\n')) - set(('LABEL', ''))
        obsolete_jobs = existing_jobs - deployed_jobs

        for job in obsolete_jobs:
            subprocess.run(
                ['oc', 'delete', 'all', '-l', f'schedule_job={job}'],
                check=False)
