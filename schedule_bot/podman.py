"""Immediate deployment of one job in a schedule with Podman."""
import itertools
import subprocess

from . import utils


class PodmanDeployment(utils.Deployment):
    """Testing the deployment of one job in a schedule with Podman."""

    # pylint: disable=too-many-arguments
    def __init__(self, schedule_config, extra_env_variables,
                 job, image=None, code_overlay=None):
        """Prepare the deployment of one job in a schedule with Podman."""
        super().__init__(schedule_config, extra_env_variables)
        self._job_name = job
        self._job_config = schedule_config['jobs'][job]
        self._image = image
        self._code_overlay = code_overlay

    def deploy(self):
        """Deploy one job in a schedule with Podman."""
        schedule_app = f'schedule-bot-{self._schedule_config["name"]}'
        schedule_job = f'{schedule_app}-{self._job_name}'

        variables = self.schedule_variables()
        variables.update(self.schedule_secrets())
        variables.update(self.job_variables(self._job_name))

        args = ['podman', 'run', '--read-only', '--tmpfs', '/data',
                '--name', schedule_job] + list(itertools.chain(
                    *[('-e', f'{k}={v}') for k, v in variables.items()]))

        if self._code_overlay:
            args += ['-v', f'{self._code_overlay}:/code']

        args += [self._image or self.image(self._job_name)]
        try:
            subprocess.run(args + self.command_line(self._job_name),
                           check=True)
        finally:
            subprocess.run(['podman', 'rm', '-f', schedule_job], check=False)
