"""Deployment of a schedule to GitLab."""
import re
from urllib.parse import urlparse
from urllib.parse import urlunparse

import gitlab
import yaml

from . import utils


class GitLabDeployment(utils.Deployment):
    """Control the deployment of a schedule to GitLab."""

    def __init__(self, schedule_config, extra_env_variables,
                 project_url, private_token):
        """Prepare the deployment of a schedule to GitLab."""
        super().__init__(schedule_config, extra_env_variables)

        parts = urlparse(project_url)
        instance_url = urlunparse(list(parts[0:2]) + [''] * 4)
        self._project_name = re.sub(r'(.git)?/*$', '', parts.path[1:])

        self._gl = gitlab.Gitlab(instance_url, private_token=private_token)
        self._gl.auth()
        self._gl_project = None

    def deploy(self):
        """Update the schedule on GitLab."""
        self._configure_project()
        self._configure_variables()
        self._configure_schedules()

    def cleanup(self):
        """Remove old jobs that are not in the schedule anymore."""
        self._clean_variables()
        self._clean_schedules()

    def _configure_project(self):
        if self._gl_project:
            return
        namespace, path = self._project_name.rsplit('/', 1)
        try:
            self._gl_project = self._gl.projects.get(self._project_name)
        except gitlab.exceptions.GitlabError:
            print(f'Creating project {self._project_name}')
            self._gl_project = self._gl.projects.create({
                'path': path,
                'namespace_id': self._gl.namespaces.get(namespace).id,
                'initialize_with_readme': True})
        payload = {
            'name': path,
            'description': f'Pipelines for {self._schedule_config["name"]}',
            'issues_access_level': 'disabled',
            'merge_requests_access_level': 'disabled',
            'wiki_access_level': 'disabled',
            'snippets_access_level': 'disabled',
            'lfs_enabled': False,
            'build_timeout': 24 * 60 * 60,
            'auto_cancel_pending_pipelines': 'disabled'
        }
        observed = self._gl_project.attributes
        if any(observed[v] != payload[v] for v in payload):
            print(f'Updating project {self._project_name}')
        for var in payload:
            if observed[var] != payload[var]:
                print(f'  {var}: {observed[var]} -> {payload[var]}')
                self._gl.projects.update(self._gl_project.id,
                                         {var: payload[var]})

    def _configure_variables(self):
        gl_variables = self._gl_map(self._gl_project.variables, 'key')

        variables = self.schedule_variables()
        for key, value in variables.items():
            self._update_project_variable(
                key, value, gl_variables.get(key), False)

        secrets = self.schedule_secrets()
        for key, value in secrets.items():
            self._update_project_variable(
                key, value, gl_variables.get(key), True)

    def _configure_schedules(self):
        gl_schedules = self._gl_map(self._gl_project.pipelineschedules,
                                    'description')
        job_tags = {}
        for job_name in self._schedule_config.get('jobs', {}):
            job_config = self._schedule_config['jobs'][job_name]

            job_tags[job_name] = job_config.get('gitlab', {}).get('tags', [])
            schedule = self._configure_schedule(
                job_name, job_config, gl_schedules)
            self._configure_schedule_variables(job_name, schedule)

        self._configure_gitlab_ci_yml(job_tags)

    def _configure_schedule(self, job_name, job_config, gl_schedules):
        if job_name in gl_schedules:
            schedule = self._gl_project.pipelineschedules.get(
                gl_schedules[job_name].id)
        else:
            print(f'Creating schedule {job_name}')
            schedule = self._gl_project.pipelineschedules.create({
                'ref': 'master',
                'description': job_name,
                'cron': job_config['schedule']})
        payload = {'cron': job_config['schedule'],
                   'active': not job_config.get('disabled'),
                   'cron_timezone': 'UTC'}
        if any(schedule.attributes[v] != payload[v] for v in payload):
            print(f'Updating schedule {job_name}')
            self._gl_project.pipelineschedules.update(schedule.id, payload)
        if schedule.owner['id'] != self._gl.user.id:
            print(f'Taking ownership of schedule {job_name}')
            schedule.take_ownership()
        return schedule

    def _configure_schedule_variables(self, job_name, schedule):
        variables = self.job_variables(job_name, data_dir=None)
        gl_variables = {v['key']: v
                        for v in schedule.attributes['variables']}
        gl_variable_names = set(gl_variables)
        for key in gl_variable_names - set(variables):
            print(f'Deleting schedule variable {key}')
            schedule.variables.delete(key)
        for key, value in variables.items():
            payload = {'key': key, 'value': value,
                       'variable_type': 'env_var'}
            gl_variable = gl_variables.get(key)
            if not gl_variable:
                print(f'Creating schedule variable {key}')
                schedule.variables.create(payload)
            elif any(gl_variable[v] != payload[v] for v in payload):
                print(f'Updating schedule variable {key}')
                schedule.variables.update(key, payload)

    def _configure_gitlab_ci_yml(self, job_tags):
        gitlab_ci_yml = {job_name: self._job_template(job_name, tags)
                         for job_name, tags in job_tags.items()}
        content = yaml.dump(gitlab_ci_yml, sort_keys=False)
        file_path = '.gitlab-ci.yml'
        payload = {
            'file_path': file_path,
            'branch': 'master',
            'content': content,
            'commit_message': 'Update configuration'}
        try:
            current = self._gl_project.files.get(
                file_path, 'master').decode().decode('utf-8')
            if current != content:
                print('Updating GitLab CI/CD pipeline configuration')
                self._gl_project.files.update(file_path, payload)
        except gitlab.exceptions.GitlabError:
            print('Creating GitLab CI/CD pipeline configuration')
            self._gl_project.files.create(payload)

    def _clean_variables(self):
        variables = self.schedule_variables()
        variables.update(self.schedule_secrets())
        gl_variables = self._gl_map(self._gl_project.variables, 'key')
        for key in set(gl_variables) - set(variables):
            print(f'Deleting project variable {key}')
            gl_variables[key].delete()

    def _clean_schedules(self):
        deployed_schedules = self._schedule_config.get('jobs', {}).keys()
        existing_schedules = self._gl_map(self._gl_project.pipelineschedules,
                                          'description')
        obsolete_schedules = set(existing_schedules) - set(deployed_schedules)
        for key in obsolete_schedules:
            print(f'Deleting schedule {key}')
            existing_schedules[key].delete()

    def _update_project_variable(self, key, value, gl_variable, masked):
        payload = {'key': key, 'value': value,
                   'variable_type': 'env_var',
                   'protected': False, 'masked': masked}
        if gl_variable is None:
            print(f'Creating project variable {key}')
            method = self._gl_project.variables.create
        elif any(gl_variable.attributes[v] != payload[v] for v in payload):
            print(f'Updating project variable {key}')
            # pylint: disable=multiple-statements
            def method(arg): return self._gl_project.variables.update(key, arg)
        else:
            return
        try:
            method(payload)
        except gitlab.exceptions.GitlabError:
            payload['masked'] = False
            method(payload)

    def _job_template(self, job_name, tags):
        return {
            'image': self.image(job_name),
            'variables': {'SCHEDULE_BOT_DATA_DIR': '$CI_PROJECT_DIR'},
            'script': ['cd /code'] + self.command_lines(job_name),
            'tags': tags,
            'except': {'variables': [
                '$CI_PIPELINE_SOURCE != "schedule"',
                f'$SCHEDULE_BOT_JOB_NAME != "{job_name}"'
            ]}
        }

    @staticmethod
    def _gl_map(manager, field):
        return {s.attributes[field]: s for s in manager.list(as_list=False)}
